# Equinox2

Equinox2 is a Javascript library for develop pattern.

## Installation

Use the node package manager [npm](https://www.npmjs.com/get-npm) to install Equinox2.

```bash
npm install equinox2
```

## Usage

```node.js
import equinox2


```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)